# dc-mobile
Exemplos de código para a plataforma Android e iOS.

Este projeto visa o desenvolvimento de material didático para a programação em dispositivos móveis.

Acesse [http://mobile.dc.ufscar.br/](http://mobile.dc.ufscar.br/) para saber mais.

Acesse [aqui](https://bytebucket.org/dc-ufscar/mobile/raw/master/doc/android/android.pdf) a versão atual do material Android.

Acesse [aqui](https://bytebucket.org/dc-ufscar/mobile/raw/master/doc/ios/ios.pdf) a versão atual do material iOS.
